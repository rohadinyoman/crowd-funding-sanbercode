<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

// use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('dateMiddleware')->group(function () {
    Route::get('/test', 'TesController@test');
});

Route::get('/test1', 'TesController@test1');

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('/admin', 'TesController@admin');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
