<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class DateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_day = Carbon::now()->day;
        if ($current_day >= 1 && $current_day <= 3) {
            return $next($request);
        }
        abort(403);
    }
}
